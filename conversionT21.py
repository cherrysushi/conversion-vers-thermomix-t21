#!/usr/bin/python

# python3
# conversion to Thermomix T21 x --> x*(3/4)

# usage:
# conversionT21.py qtt1 qtt2 qtt3
# or
# conversionT21.py

import sys

arg = 0

if (sys.argv[1:]):
  arg = sys.argv[1:]

else:
  n = str(input("Input quantities using space as a separator then [Return] \n"))
  arg = n.split(' ')

for qtt in arg:
    after = "%.2f" % (float(qtt)*(3/4))
    print("original qtt: "+str(qtt)+" --> "+str(after))

print("done.\n")

